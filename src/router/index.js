import { createRouter, createWebHashHistory } from "vue-router";
import HomeView from "@/views/HomeView.vue";

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      component: HomeView,
      children: [
        {
          path: "/create",
          name: "createSession",
          props: true,
          component: () => import("@/components/CreateSession.vue"),
        },
        {
          path: "/join",
          name: "joinSession",
          props: true,
          component: () => import("@/components/JoinSession.vue"),
        },
        {
          path: "",
          name: "home",
          props: true,
          component: () => import("@/components/LandingDetails.vue"),
        },
      ],
    },
    {
      path: "/session/:sessionId",
      name: "session",
      props: true,
      component: () => import("@/views/SessionView.vue"),
    },
    {
      path: "/no-session",
      name: "noSession",
      props: true,
      component: () => import("@/views/NoSession.vue"),
    },
    {
      path: "/our-thoughts",
      name: "ourThoughts",
      props: true,
      component: () => import("@/views/OurThoughts.vue"),
    },
    {
      path: "/privacy-policy",
      name: "PrivacyPolicy",
      component: () => import("@/views/PrivacyPolicy.vue"),
    },
  ],
  scrollBehavior() {
    return { top: 0 };
  },
});

export default router;
