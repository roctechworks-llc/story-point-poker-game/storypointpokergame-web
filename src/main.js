import "./assets/styles/main.css";

import { createApp } from "vue";
import store from "@/stores";

import App from "@/App.vue";
import router from "@/router";

const app = createApp(App);

app.use(router);
app.use(store);

app.mount("#app");

console.warn(
  "\n\n\nSomething about don't paste code into here, blah blah blah. No need to worry, we don't collect any data. Poke around as much as you want. 😉\n\n\n",
);
