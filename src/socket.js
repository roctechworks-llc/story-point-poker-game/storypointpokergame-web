import { io } from "socket.io-client";

const URL =
  // eslint-disable-next-line
  process.env.NODE_ENV === "production"
    ? "//api.storypointpokergame.com"
    : "http://localhost:3000";

export const socket = io(URL, {
  secure: true,
  autoConnect: false,
});
