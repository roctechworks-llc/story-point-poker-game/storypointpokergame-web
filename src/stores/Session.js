import { defineStore } from "pinia";
import { socket } from "@/socket";
import { useInfoStore } from "./Info";

export const DECK_TYPE_COMMON = 1;
export const DECK_TYPE_FIBONACCI = 2;

const CARD_MAPPING = {
  [DECK_TYPE_COMMON]: [0, 0.5, 1, 2, 3, 5, 8, 13, 20, "?"],
  [DECK_TYPE_FIBONACCI]: [0, 1, 3, 5, 8, 13, 21, "?"],
};

export const useSessionStore = defineStore("session", {
  state: () => {
    return {
      sessionId: null,
      userId: null,
      creating: false,
      joining: false,
      voting: false,
      deckOption: DECK_TYPE_COMMON,
      moderatorId: null,
      moderatorName: null,
      castedVotes: 0,
      votableGuests: 0,
      selected: null,
      revealing: false,
      revealed: false,
      metrics: {
        min: 0,
        max: 0,
        mode: [],
        itemized: [],
      },
    };
  },
  getters: {
    deckValues: (state) => CARD_MAPPING[state.deckOption],
    deckSelectOptions: () => [
      {
        value: DECK_TYPE_COMMON,
        label: "Common",
      },
      {
        value: DECK_TYPE_FIBONACCI,
        label: "Fibonacci",
      },
    ],
    sessionUrl: (state) =>
      `https://storypointpokergame.com/#/session/${state.sessionId}`,
    moderator: (state) => state.moderatorId === state.userId,
  },
  actions: {
    bindEvents() {
      socket.off("session:created");
      socket.off("session:joined");
      socket.off("session:guest-change");
      socket.off("session:voted");
      socket.off("session:revealed");
      socket.off("session:left");
      socket.off("session:none");

      socket.on("session:created", (res) => {
        const { nickname } = useInfoStore();
        this.sessionId = res.id;
        this.userId = res.userId;
        this.moderatorId = res.userId;
        this.moderatorName = nickname;
        this.router.push(`/session/${this.sessionId}`);
      });

      socket.on("session:joined", (res) => {
        this.sessionId = res.id;
        this.userId = res.userId;
        this.router.push(`/session/${this.sessionId}`);
      });

      socket.on("session:guest-change", (res) => {
        const {
          moderatorId,
          moderatorNickname,
          deckTypeId,
          castedVotes,
          votableGuests,
        } = res;
        this.moderatorId = moderatorId;
        this.moderatorName = moderatorNickname;
        this.deckOption = deckTypeId;
        this.castedVotes = castedVotes;
        this.votableGuests = votableGuests;
      });

      socket.on("session:voted", (res) => {
        const { castedVotes, votableGuests } = res;
        this.castedVotes = castedVotes;
        this.votableGuests = votableGuests;
      });

      socket.on("session:revealed", (res) => {
        const { min, max, mode, itemized } = res;
        this.revealed = true;
        this.castedVotes = 0;
        this.metrics = {
          min,
          max,
          mode,
          itemized,
        };
      });

      // Intentionally left. Definitely to join another.
      socket.on("session:left", () => {
        this.router.push("/");
      });

      // No session found.
      socket.on("session:none", () => {
        this.router.push(`/no-session`);
      });
    },
    createSession() {
      this.creating = true;
      const { nickname } = useInfoStore();
      socket.emit("session:create", {
        username: nickname,
        deckTypeId: this.deckOption,
      });
      this.creating = false;
    },
    joinSession() {
      this.joining = true;
      socket.emit("session:join", {
        session: this.sessionId,
        user: this.userId,
      });
      this.joining = false;
    },
    castVote() {
      this.voting = true;
      socket.emit("session:vote", {
        vote: this.selected,
        session: this.sessionId,
        user: this.userId,
      });
      this.voting = false;
    },
    revealSession() {
      this.revealing = true;
      socket.emit("session:reveal", {
        session: this.sessionId,
        user: this.userId,
      });
    },
    leaveSession() {
      socket.off("session:guest-change");
      socket.emit("session:leave", {
        session: this.sessionId,
        user: this.userId,
      });

      this.sessionId = null;
      this.moderatorId = null;
      this.moderatorName = null;
    },
    getDeckValues(type = DECK_TYPE_COMMON) {
      if (!(type in CARD_MAPPING)) {
        type = DECK_TYPE_COMMON;
      }
      return CARD_MAPPING[type];
    },
  },
  persist: {
    paths: ["userId", "sessionId"],
  },
});
