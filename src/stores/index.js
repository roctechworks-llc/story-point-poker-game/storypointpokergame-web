import { markRaw } from "vue";
import { createPinia } from "pinia";
import piniaPluginPersistedstate from "pinia-plugin-persistedstate";
import router from "@/router";

import "@/stores/Info";
import "@/stores/Session";

const store = createPinia();
store.use(({ store: piniaStore }) => (piniaStore.router = markRaw(router)));
store.use(piniaPluginPersistedstate);

export default store;
