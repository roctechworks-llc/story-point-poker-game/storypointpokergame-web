import { defineStore } from "pinia";

export const MIN_LENGTH = 5;
export const MAX_LENGTH = 25;

export const useInfoStore = defineStore("info", {
  state: () => {
    return {
      nickname: null,
    };
  },
  getters: {
    nameIsInvalid: (state) =>
      !!state.nickname && state.nickname.length > MAX_LENGTH,
  },
  actions: {},
  persist: true,
});
