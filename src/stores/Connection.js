import { defineStore } from "pinia";
import { socket } from "@/socket";

export const useConnectionStore = defineStore("connection", {
  state: () => ({
    connecting: true,
    isConnected: false,
  }),
  actions: {
    bindEvents() {
      socket.off("connect");
      socket.off("site:vist");
      socket.off("connect:error");
      socket.off("disconnect");

      socket.on("connect", () => {
        this.isConnected = true;
        this.connecting = false;
      });

      socket.on("site:visit", () => {
        this.isConnected = true;
        this.connecting = false;
      });

      socket.on("connect:error", () => {
        this.isConnected = false;
        this.connecting = false;
      });

      socket.on("disconnect", () => {
        this.isConnected = false;
        this.connecting = false;
      });
    },
    connect() {
      try {
        this.bindEvents();
        socket.connect();
        this.isConnected = true;
      } catch (e) {
        console.error(e);
        this.isConnected = false;
      } finally {
        this.connecting = false;
      }
    },
  },
});
