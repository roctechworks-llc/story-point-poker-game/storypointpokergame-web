# storypointpokergame-web

How to get started with this project.

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```
or

```sh
bun install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```
or

```sh
bun run dev
```

### Compile and Minify for Production

```sh
npm run build
```
or

```sh
bun run dev
```
